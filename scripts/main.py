#!/usr/bin/env python
from datetime import datetime
from qlbot import *
import record_data as dt
import time

x = None

def main():

	rospy.init_node("qlbot", anonymous = True)

	global x ; x = QLbot(LoadName)	

	if not x.start : return None

	while not rospy.is_shutdown():
		
		isEnd = None

		while x.state != (-1, -1) :
			
			print("------------------------- Episode: %d -------------------------" % int(x.Episode+1))

			if isEnd is True or isEnd is None : x.state = x.GetCurrentState()
			else: x.state = state_p


			action, action_policy = x.ChooseAction(x.state)

			isEnd, state_p, reward = x.MoveToNextStep(action, action_policy)

			print( "Main: ", x.state, state_p )

			x.updateQ(x.state, action, reward, state_p)
			


if __name__ == '__main__':
	main()	

"""
try:
	if __name__ == '__main__':
		main()	

except (KeyboardInterrupt, SystemExit, TypeError):

	print( color_R + " \nProgram Terminated " + color_BP )
	time_ = time.strftime("%Y-%m-%d_%H-%M-%S", time.localtime())
	dt.WriteData( x, time_ + 'TbE' , [['Terminated by Exception']] )
	dt.WriteLog(  x, time_ + 'TbE' , x.Episode+1 , x.step, -0.0)


"""
